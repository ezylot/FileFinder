package at.ezylot.Benchmark;

import at.ezylot.FileFinder;
import at.ezylot.RecursiveFileFinder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Benchmark {

    private static final String base = "bechmarkDir";


    /**
     * This benchmark tries the performance of the FileFinder by creating a (relativly) big
     * directory structure and meassuring the time needed to find a file.
     *
     * The execution will, obviously, take some time.
     */
    public static void main(String[] args) throws IOException {

        FileFinder finder = new RecursiveFileFinder();
        List<String> result;

        //Start timer
        long start = System.nanoTime();

        // Remove old structure if it exists, because it might be not valid anymore
        deleteDirectory(new File(base));

        // Create the directory structure
        System.out.println("Creating test directory structure");
        int l = 2;
        File fl = null;

        for(int e = 0; e < l; e++) {
            System.out.println("\t"+e);
            for (int z = 0; z < l; z++) {
                System.out.println("\t\t"+z);
                for (int d = 0; d < l; d++) {
                    System.out.println("\t\t\t"+d);
                    for (int v = 0; v < l; v++) {
                        System.out.println("\t\t\t\t"+v);
                        for (int f = 0; f < l; f++) {
                            for (int s = 0; s < l; s++) {
                                for (int si = 0; si < l; si++) {
                                    for (int a = 0; a < l; a++) {
                                        for (int n = 0; n < l; n++) {
                                            for (int file = 0; file < 10; file++) {
                                                fl = new File(base + "/dir" + e + "/dir" + z + "/dir" + d + "/dir" + v + "/dir" + f + "/dir" + s + "/dir" + si + "/dir" + a + "/dir" + n + "/file" + file);
                                                fl.getParentFile().mkdirs();
                                                Files.createFile(fl.toPath());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        System.out.println("Directory created after " + (System.nanoTime() - start) / 1e6 + " milliseconds");
        start = System.nanoTime();

        result = finder.findFile(base, "file1");

        System.out.println("Found all file1 after " + (System.nanoTime() - start) / 1e6 + " milliseconds");
        System.out.println("There were " + result.size() + " occurences");
        start = System.nanoTime();

        result = finder.findFile(base, "file3");
        System.out.println("Found all file3 after " + (System.nanoTime() - start) / 1e6 + " milliseconds");
        start = System.nanoTime();

        result = finder.findFile(base, "file6");
        System.out.println("Found all file6 after " + (System.nanoTime() - start) / 1e6 + " milliseconds");

        result = finder.findFile(base, "file9");
        System.out.println("Found all file9 after " + (System.nanoTime() - start) / 1e6 + " milliseconds");
        start = System.nanoTime();

        System.out.println("Deleting directory structure");
        deleteDirectory(new File(base));

        System.out.println("Done");
    }

    private static boolean deleteDirectory(File path) {
        if( path.exists() ) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                if(files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                }
                else {
                    files[i].delete();
                }
            }
        }
        return( path.delete() );
    }
}
