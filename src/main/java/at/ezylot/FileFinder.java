package at.ezylot;

import java.util.List;

public interface FileFinder {

    /**
     * Performs a recursive find in the current basedir looking for the filename.
     * Returns a list of paths relative from the basedir, or an empty list if no match was found.
     *
     * @param basedir  Base directory that is used as the root directory for the search
     * @param filename Filename of the searched file
     * @return Returns a list with the (from the basedir relative) paths of the matches
     */
    public List<String> findFile(String basedir, String filename) throws IllegalArgumentException;

    /**
     * Performs a recursive find in the current basedir looking for a directory with the given name.
     * Returns a list of paths relative from the basedir, or an empty list if no match was found.
     *
     * @param basedir       Base directory that is used as the root directory for the search
     * @param directoryName Directory name that should be searched for
     * @return Returns a list with the (from the basedir relative) paths of the matches
     */
    public List<String> findDirectory(String basedir, String directoryName) throws IllegalArgumentException;
}
