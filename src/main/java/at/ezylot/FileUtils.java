package at.ezylot;

public class FileUtils {
    public static Boolean checkFileName(String filename) {
        return !(filename.matches(".*[<>:\"/\\\\|\\?\\*].*"));
    }

    public static Boolean checkDirectoryName(String directoryname) {
        return !(directoryname.contains("/"));
    }
}
