package at.ezylot;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class RecursiveFileFinder implements FileFinder {

    @Override
    public List<String> findFile(String basedir, String filename) {
        if(basedir == null || basedir.isEmpty() || filename == null || filename.isEmpty())
            throw new IllegalArgumentException("Arguments can not be empty");

        File base = new File(basedir);
        if(!base.exists())
            throw new IllegalArgumentException("Base directory is not valid");

        if(!base.isDirectory())
            throw new IllegalArgumentException("Base directory is not a directory");

        if(!FileUtils.checkFileName(filename))
            throw new IllegalArgumentException("Filename does contain illegal characters");

        List<String> solutions = new ArrayList<>();
        solutions.addAll(findFile(basedir, filename, 0));

        for(int i = 0; i < solutions.size(); i++) {
            solutions.set(i, solutions.get(i).substring(basedir.length() + 1));
        }

        return solutions;
    }

    public List<String> findFile(String basedir, String filename, Integer depth) {
        List<String> solutions = new ArrayList<>();

        File base = new File(basedir);
        for (String node : base.list()) {
            File f = new File(basedir, node);
            if (f.isDirectory()) {
                solutions.addAll(findFile(f.getPath(), filename, depth+1));
            } else {
                if (node.equals(filename)) {
                    solutions.add(new File(basedir, node).toString());
                }
            }
        }

        return solutions;
    }

    @Override
    public List<String> findDirectory(String basedir, String directoryName) {
        if(basedir == null || basedir.isEmpty() || directoryName == null || directoryName.isEmpty())
            throw new IllegalArgumentException("Arguments can not be empty");

        File base = new File(basedir);
        if(!base.exists())
            throw new IllegalArgumentException("Base directory is not valid");

        if(!base.isDirectory())
            throw new IllegalArgumentException("Base directory is not a directory");

        if(!FileUtils.checkDirectoryName(directoryName))
            throw new IllegalArgumentException("Directory name does contain illegal characters");


        List<String> solutions = new ArrayList<>();

        solutions.addAll(findDirectory(basedir, directoryName, 0));
        for(int i = 0; i < solutions.size(); i++) {
            solutions.set(i, solutions.get(i).substring(basedir.length() + 1));
        }

        return solutions;
    }

    public List<String> findDirectory(String basedir, String directoryName, Integer depth) {
        List<String> solutions = new ArrayList<>();

        File base = new File(basedir);
        for (String node : base.list()) {
            File f = new File(basedir, node);
            if (f.isDirectory()) {
                if(node.equals(directoryName))
                    solutions.add(new File(basedir, node).toString());
                solutions.addAll(findDirectory(f.getPath(), directoryName, depth + 1));
            }
        }

        return solutions;
    }
}
