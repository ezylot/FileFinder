package at.ezylot;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@RunWith(value = Parameterized.class)
public class FileFinderTest {

    public String basedir = "testDir";
    public FileFinder finder;

    @Parameterized.Parameters
    public static Collection<FileFinder[]> data() {
        List finders = new ArrayList<FileFinder[]>();

        finders.add(new FileFinder[]{new RecursiveFileFinder()});

        return finders;
    }

    public FileFinderTest(FileFinder finderInstance) throws Exception {
        this.finder = finderInstance;
        basedir = finder.toString() + "@" + basedir;
    }

    /*
    * Creates a directory structure for the unit tests. It as following structure
    *   + testDir
    *      + sub1
    *        + sub2
    *          - a.txt
    *          - a
    *          - b.txt
    *      + b
    *        +sub2
    *        - c
    *        - b.awd
    *        - b
    *      - a.txt
    *      - b.asdfg
    *      - c
    * */
    @Before
    public void InitializeTestDirectoryStructure() throws IOException {
        delete(new File(basedir));

        Files.createDirectory(new File(basedir).toPath());
        Files.createDirectory(new File(basedir + "/sub1").toPath());
        Files.createDirectory(new File(basedir + "/sub1/sub2").toPath());
        Files.createDirectory(new File(basedir + "/b").toPath());
        Files.createDirectory(new File(basedir + "/b/sub2").toPath());

        Files.createFile(new File(basedir + "/a.txt").toPath());
        Files.createFile(new File(basedir + "/b.asdfg").toPath());
        Files.createFile(new File(basedir + "/c").toPath());

        Files.createFile(new File(basedir + "/sub1/sub2/a.txt").toPath());
        Files.createFile(new File(basedir + "/sub1/sub2/a").toPath());
        Files.createFile(new File(basedir + "/sub1/sub2/b").toPath());

        Files.createFile(new File(basedir + "/b/c").toPath());
        Files.createFile(new File(basedir + "/b/b.awd").toPath());
        Files.createFile(new File(basedir + "/b/b").toPath());
    }

    @After
    public void deleteTestDirectoryStructure() throws FileNotFoundException {
        delete(new File(basedir));
    }

    /* ------------------------------------------------------------------------------------------------- */
    /* ----------------------------------- Test method: findFile --------------------------------------- */
    /* ------------------------------------------------------------------------------------------------- */

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyRootDir() {
        finder.findFile("", "b");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalRootDir() {
        finder.findFile("213", "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyFileName() {
        finder.findFile(basedir, "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithFileAsBaseDir() {
        finder.findFile(basedir + "/c", "c");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFileNameWithGreaterSign() {
        finder.findFile(basedir, "ad>wad");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFileNameWithLesserSign() {
        finder.findFile(basedir, "ad<wad");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFileNameWithColon() {
        finder.findFile(basedir, "ad:wad");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFileNameWithQuote() {
        finder.findFile(basedir, "ad\"wad");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFileNameWithSlash() {
        finder.findFile(basedir, "ad/wad");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFileNameWithBackslash() {
        finder.findFile(basedir, "ad\\wad");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFileNameWithPipe() {
        finder.findFile(basedir, "ad|wad");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFileNameWithQuestionMark() {
        finder.findFile(basedir, "ad?wad");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFileNameWithAsterisk() {
        finder.findFile(basedir, "ad*wad");
    }

    @Test
    public void testNonExistingFiles() {
        Assert.assertEquals(0, finder.findFile(basedir, "nonexisting").size());
    }

    @Test
    public void findFileInRootDirectory() {
        Assert.assertEquals(1, finder.findFile(basedir, "b.asdfg").size());
        Assert.assertEquals("b.asdfg", finder.findFile(basedir, "b.asdfg").get(0));
    }

    @Test
    public void findFileInSubDirectory() {
        Assert.assertEquals(1, finder.findFile(basedir, "b.awd").size());
        Assert.assertEquals(new File("b/b.awd").toString(), finder.findFile(basedir, "b.awd").get(0));
    }

    @Test
    public void findMultipleFilesInSubDirs() {
        Assert.assertEquals(2, finder.findFile(basedir, "a.txt").size());
        Assert.assertTrue(finder.findFile(basedir, "a.txt").containsAll(Arrays.asList("a.txt", new File("sub1/sub2/a.txt").toString())));
    }

    /* ------------------------------------------------------------------------------------------------- */
    /* ----------------------------------- Test method: findDirectory ---------------------------------- */
    /* ------------------------------------------------------------------------------------------------- */

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyRootDirInDirectory() {
        finder.findDirectory("", "sub1");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalRootDirInDirectory() {
        finder.findDirectory("512", "sub1");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyDirectoryName() {
        finder.findDirectory(basedir, "");
    }

    @Test
    public void testFindNonExistingDirectory() {
        Assert.assertEquals(0, finder.findDirectory(basedir, "nonexisting").size());
    }

    @Test
    public void findDirectoryInRoot() {
        Assert.assertEquals(1, finder.findDirectory(basedir, "sub1").size());
        Assert.assertEquals("sub1", finder.findDirectory(basedir, "sub1").get(0));
    }

    @Test
    public void findDirectoryRecursively() {
        Assert.assertEquals(2, finder.findDirectory(basedir, "sub2").size());
        Assert.assertTrue(finder.findDirectory(basedir, "sub2").containsAll(Arrays.asList(new File("sub1/sub2").toString(), new File("b/sub2").toString())));
    }

    private void delete(File f) throws FileNotFoundException {
        if (f.exists() == false) return;

        if (f.isDirectory()) {
            for (File c : f.listFiles())
                delete(c);
        }
        if (!f.delete())
            throw new FileNotFoundException("Failed to delete file: " + f);
    }
}
